import java.util.List;

public class Tester implements Angajat{

    long CNP;
    String name, surname;
    boolean AutomateTesting;

    public Tester(long CNP, String name, String surname, boolean AutomateTesting)
    {
        this.CNP = CNP;
        this.name = name;
        this.surname = surname;
        this.AutomateTesting = AutomateTesting;
    }

    public String getString(){
        return "[Tester] CNP:"+this.CNP+"  Nume: "+this.name+ " "+this.surname+"   Testare Automata: "+this.AutomateTesting;
    }

    public long getCNP() {
        return CNP;
    }

    public void setCNP(long CNP) {
        this.CNP = CNP;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

     public boolean getAutomateTesting() {
        return AutomateTesting;
    }


    public void setAutomateTesting(boolean automateTesting) {
        AutomateTesting = automateTesting;
    }

    public List<Angajat> getSubordinates() {
        return null;
    }

     public String[] getKnownLanguages() {
        return new String[0];
    }

    public String toString() {
        return "Tester{" +
                "CNP=" + CNP +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", AutomateTesting=" + AutomateTesting +
                '}';
    }
}
