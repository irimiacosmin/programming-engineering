import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Programator implements Angajat {

    long CNP;
    String name,surname;
    String[] knownLanguages;

    public Programator(long CNP, String name, String surname, String[] knownLanguages){
        this.CNP = CNP;
        this.name = name;
        this.surname = surname;
        this.knownLanguages = knownLanguages;
    }

    
    public long getCNP() {
        return CNP;
    }

    public void setCNP(long CNP) {
        this.CNP = CNP;
    }

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    
    public String[] getKnownLanguages() {
        return knownLanguages;
    }

    public void setKnownLanguages(String[] knownLanguages) {
        this.knownLanguages = knownLanguages;
    }

    public List<Angajat> getSubordinates() {
        return null;
    }

    
    public boolean getAutomateTesting() {
        return false;
    }

    
    public String toString() {
        return "Programator{" +
                "CNP=" + CNP +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", knownLanguages=" + Arrays.toString(knownLanguages) +
                '}';
    }

}
