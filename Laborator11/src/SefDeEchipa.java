
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SefDeEchipa implements Angajat {

    long CNP;
    String name,surname;
    List<Angajat> subordinates;


    public SefDeEchipa(long CNP, String name, String surname, List<Angajat> subordinates){

        this.CNP = CNP;
        this.name = name;
        this.surname = surname;
        this.subordinates = subordinates;
    }

    public String getString(){

        String str = "[Sef de echipa] CNP:"+this.CNP+"  Nume: "+this.name+ " "+this.surname+"   Subordonati: [ ";
        for(Angajat limbaj:subordinates){
            str = str+limbaj.getName()+" "+limbaj.getSurname()+"("+limbaj.getCNP()+")  ";
        }
        return str+"]";
    }

    
    public long getCNP() {
        return CNP;
    }

    public void setCNP(long CNP) {
        this.CNP = CNP;
    }

    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    
    public List<Angajat> getSubordinates() {
        return subordinates;
    }

    public void setSubordinates(List<Angajat> subordinates) {
        this.subordinates = subordinates;
    }

    
    public String[] getKnownLanguages() {
        return new String[0];
    }

    
    public boolean getAutomateTesting() {
        return false;
    }

    
    public String toString() {
        return "Sef_de_Echipa{" +
                "CNP=" + CNP +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", subordinates=" + subordinates +
                '}';
    }


}
