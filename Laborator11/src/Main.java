
import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class Main {

    public static List<Angajat> listaAngajati = new ArrayList<Angajat>();

    public static void main(String[] args){

      Ex1();

    }

    public static void Ex1(){

        Tester t1 = new Tester(2971,"Trinca","Ioana-Alexandra",true);
        Tester t2 = new Tester(2972,"Popescu","Ion",false);
        Tester t3 = new Tester(2973,"Adamescu","Florin",true);

        Programator p1 = new Programator(1971,"Matei","Madalin", new String[]{"Java", "C++"});
        Programator p2 = new Programator(1972,"Ouatu","Bogdan", new String[]{"Java", "C++","Python"});
        Programator p3 = new Programator(1974,"Pohomohaci","Dumitru", new String[]{"C++"});
        Programator p4 = new Programator(1975,"Worst","Developer", new String[]{"PHP"});


        SefDeEchipa s1 = new SefDeEchipa(1973,"Irimia","Cosmin-Iulian",new ArrayList<Angajat>() {{add(t1);add(p1);add(p3);}});

        SefDeEchipa s2 = new SefDeEchipa(1976,"Bucevschi","Alexandru-Gabriel",new ArrayList<Angajat>() {{add(t2);add(t3);add(p2);add(p4);}});

        SefDeGrupa boss = new SefDeGrupa(1978,"Vintur","Cristian",new ArrayList<Angajat>() {{add(s2);add(s1);}});

        Manager patron = new Manager(2000,"Iftene","Adrian",new ArrayList<Angajat>() {{add(boss);}});



        System.out.println(t1.toString());
        System.out.println(t2.toString());
        System.out.println(t3.toString());
        System.out.println(p1.toString());
        System.out.println(p2.toString());
        System.out.println(p3.toString());
        System.out.println(p4.toString());
        System.out.println(s1.toString());
        System.out.println(s2.toString());
        System.out.println(boss.toString());
        System.out.println(patron.toString());

        listaAngajati.add(t1);
        listaAngajati.add(t2);
        listaAngajati.add(t3);
        listaAngajati.add(p1);
        listaAngajati.add(p2);
        listaAngajati.add(p3);
        listaAngajati.add(p4);
        listaAngajati.add(s1);
        listaAngajati.add(s2);
        listaAngajati.add(boss);

        for(Angajat angajat:listaAngajati)
                new Serializare(angajat);

        Angajat a = Serializare.Deserializare("1978.ser");
        System.out.println(a.toString());



    }



}
