
import java.io.Serializable;
import java.util.List;

public class Manager implements Angajat{

    long CNP;
    String name,surname;
    List<Angajat> subordinates;


    public Manager(long CNP, String name, String surname, List<Angajat> subordinates){

        this.CNP = CNP;
        this.name = name;
        this.surname = surname;
        this.subordinates = subordinates;
    }

    public String getString(){
        String str = "[ZEU] CNP:"+this.getCNP()+"  Nume: "+this.getName()+ " "+this.getSurname()+"   Subordonati: TOATA LUMEA!!! \n";

        str = str + "Subordonati: [ ";
        for(Angajat limbaj:subordinates){
            str = str+limbaj.getName()+" "+limbaj.getSurname()+"("+limbaj.getCNP()+")  ";
        }
        return str+"]";

    }

    @Override
    public long getCNP() {
        return CNP;
    }

    public void setCNP(long CNP) {
        this.CNP = CNP;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public List<Angajat> getSubordinates() {
        return subordinates;
    }

    public void setSubordinates(List<Angajat> subordinates) {
        this.subordinates = subordinates;
    }

    @Override
    public String[] getKnownLanguages() {
        return new String[0];
    }

    @Override
    public boolean getAutomateTesting() {
        return false;
    }

    @Override
    public String toString() {
        return "Manager{" +
                "CNP=" + CNP +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", subordinates=" + subordinates +
                '}';
    }

}