
import java.io.*;
import java.io.Serializable;

public class Serializare implements Serializable {

    public Serializare(Angajat angajat) {
        try {
            FileOutputStream fos = new FileOutputStream(angajat.getCNP()+".ser");
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(angajat);
            oos.close();
            fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Angajat Deserializare(String name) {
        Angajat angajat = null;
        try
        {
            FileInputStream fis = new FileInputStream(name);
            ObjectInputStream ois = new ObjectInputStream(fis);
            angajat = (Angajat) ois.readObject();
            fis.close();
            ois.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return angajat;
    }
}