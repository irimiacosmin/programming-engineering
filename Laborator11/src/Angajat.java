
import java.io.Serializable;
import java.util.List;

public interface Angajat extends Serializable {

    long getCNP();
    String getName();
    String getSurname();
    boolean getAutomateTesting();
    List<Angajat> getSubordinates();
    String[] getKnownLanguages();

}
