package com.company.game;

import com.company.scrabble.Words;

import java.io.IOException;

public class Main {

    public static void main(String args[]) throws IOException {
        Game game = new Game();
        new Words();
        game.setBag(new Bag());
        game.setBoard(new Board());
        game.addPlayer(new ScrabblePlayer("Player 1"));
        game.addPlayer(new ScrabblePlayer("Player 2"));
        game.addPlayer(new ScrabblePlayer("Player 3"));
        game.start();
    }
}
