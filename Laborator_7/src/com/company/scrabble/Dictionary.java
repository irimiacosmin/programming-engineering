package com.company.scrabble;

public interface Dictionary {

    boolean containsWords(String str);
}
