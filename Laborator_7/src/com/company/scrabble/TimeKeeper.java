package com.company.scrabble;

import java.util.List;

public class TimeKeeper implements Runnable {

    public List<Thread> threadList = null;
    long startTime;

    public TimeKeeper(){
        startTime = System.currentTimeMillis();
    }

    public void addThreadList(List<Thread> threadList){
        this.threadList = threadList;
    }

    public void run() {

        while(threadList == null){
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        boolean ready = false;
        while(ready==false) {
            ready = true;
            for (Thread thread : threadList) {
                if(thread.getState()!=Thread.State.TERMINATED){
                    ready = false;
                }
            }
        }

        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;

        System.out.print("Threads runned for ");
        System.out.print(elapsedTime);
        System.out.print(" miliseconds.");
    }


}
