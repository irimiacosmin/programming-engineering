package com.company.scrabble;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Words implements Dictionary {

    List<String>words = new ArrayList<String>();

    public Words() throws IOException {

        FileReader fr = new FileReader("src\\words.txt");

        BufferedReader br = new BufferedReader(fr);

        for(String line; (line = br.readLine()) != null; ) {

            if(line.length() < 8) {

                words.add(line.toLowerCase());
            }
        }

//        System.out.println(words);

    }

    public synchronized List<String> getWords() {
        return words;
    }

    @Override
    public synchronized boolean containsWords(String str) {

        if(words.contains(str)) {

            return true;
        }
        return false;
    }
}
