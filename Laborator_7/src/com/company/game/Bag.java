package com.company.game;

import java.util.*;

public class Bag {
    private final Queue<Character> letters = new LinkedList<>();
    public Bag() {

        fillTheBag();
    }
        public synchronized List<Character> extractLetters(int howMany) {
            // Replace the dots so that the bag is thread-safe
            List<Character> extracted = new ArrayList<>();
            for (int i = 0; i < howMany; i++) {
                if (letters.isEmpty()) break;
                extracted.add(letters.poll());
            }
            return extracted;
        }
//
//    public synchronized List<Character> getLetters() {
//        return letters;
//    }

    private synchronized void addChar(Character character, int howMany) {

        for(int i = 0; i < howMany; i++) {

            letters.add(character);

        }

    }

    private synchronized void fillTheBag() {

        addChar('e', 12);
        addChar('a', 9);
        addChar('i', 9);
        addChar('o', 8);
        addChar('n', 6);
        addChar('t', 6);
        addChar('r', 6);
        addChar('l', 4);
        addChar('s', 4);
        addChar('u', 4);
        addChar('d', 4);
        addChar('g', 3);
        addChar('b', 2);
        addChar('c', 2);
        addChar('m', 2);
        addChar('p', 2);
        addChar('f', 2);
        addChar('h', 2);
        addChar('v', 2);
        addChar('w', 2);
        addChar('y', 2);
        addChar('k', 1);
        addChar('j', 1);
        addChar('x', 1);
        addChar('q', 1);
        addChar('z', 1);

        // truly random
        Collections.shuffle((List<?>) letters, new Random(System.nanoTime()));

    }

    public synchronized Queue<Character> getLetters() {
        return letters;
    }
}
