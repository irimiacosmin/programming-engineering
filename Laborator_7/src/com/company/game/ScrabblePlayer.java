package com.company.game;

public class ScrabblePlayer extends Player {

    public ScrabblePlayer(String name) {
        super(name);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public Game getGame() {
        return super.getGame();
    }

    @Override
    public void setGame(Game game) {
        super.setGame(game);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public void run() {
        super.run();
    }
}
