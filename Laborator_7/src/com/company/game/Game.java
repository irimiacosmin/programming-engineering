package com.company.game;

import com.company.scrabble.TimeKeeper;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;

public class Game {

    private Bag bag;
    private Board board;
    private Dictionary dictionary;
    private final List<Player> players = new ArrayList<>();
    private final List<Thread> threads = new ArrayList<>();

    // genereaza constructorul null
    public Game() {
    }

    public synchronized void addPlayer(Player player) {
        players.add(player);
        player.setGame(this);
    }

    public synchronized void start() {


        TimeKeeper timeKeeper = new TimeKeeper();
        Thread t = new Thread(timeKeeper);
        t.setDaemon(true);
        t.start();
        System.out.println("Daemon started");

        for(Player player: players){
            threads.add(new Thread(player));
        }

        for(Thread thread: threads){
            thread.start();
        }

        System.out.println("All threads started");
        timeKeeper.addThreadList(threads);



    }

    public Bag getBag() {
        return bag;
    }

    public void setBag(Bag bag) {
        this.bag = bag;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public Dictionary getDictionary() {
        return dictionary;
    }

    public void setDictionary(Dictionary dictionary) {
        this.dictionary = dictionary;
    }

    public List<Player> getPlayers() {
        return players;
    }

    //Create the method that will start the game: start one thread for each player
}
