package com.company.game;

import java.util.ArrayList;
import java.util.List;

public class Board {

    private List<String> wordList;

    public Board() {

        wordList = new ArrayList<String>();

    }

    public synchronized void addWord(Player player, String word) {

        wordList.add(word);
        System.out.println("Player " + player.getName() + " submitted the word " + word + " totaling x points.");

    }

    public synchronized List<String> getWordList() {
        return wordList;
    }

    public synchronized void setWordList(List<String> wordList) {
        this.wordList = wordList;
    }
}
