package com.company.game;

import com.company.scrabble.Combination;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public abstract class Player implements Runnable {

    private List<Character> extracted;
    private String name;
    private Game game;
    Random random = new Random();

    public Player(String name) {
        this.name = name;
        extracted = new ArrayList<Character>();
    }

    private synchronized boolean createSubmitWord() throws InterruptedException, IOException {

        if(extracted.size() != 7) {

            extracted.addAll(game.getBag().extractLetters(7-extracted.size()));
        }
        else {

            extracted = game.getBag().extractLetters(7);
        }
        if (extracted.isEmpty()) {
            return false;
        }

        //System.out.println(extracted.size() + " " + extracted);

        for(int i = extracted.size(); i > 1; i--) {

            Combination combination = new Combination(extracted, i);

            List<String> validWords = combination.getValidWords();

            if(!validWords.isEmpty()) {

                String chosenWord = validWords.get(random.nextInt(validWords.size()));
                for(char c : chosenWord.toCharArray()) {

                    extracted.remove((Character) c);

                }

                game.getBoard().addWord(this, chosenWord);
                Thread.sleep(300);
                return true;




            }
//            System.out.println(i + " " + validWords);

        }
        return false;

    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public void run() {

        while(!game.getBag().getLetters().isEmpty()) {
            try {
                createSubmitWord();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
