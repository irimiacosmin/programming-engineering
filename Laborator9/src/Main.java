

import Bonus.BarChartReport;
import Bonus.HTMLReporter;
import Compulsory.AlbumController;
import Compulsory.ArtistController;
import Compulsory.Database;
import Optional.Album;
import Optional.Artist;
import Optional.Chart;
import Optional.EntityController;
import com.github.javafaker.Faker;

import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

public class Main {

    public static List<Album> bigAlbumList;
    public static List<Artist> bigArtistList;
    public static List<Chart> bigChartList;
    public static List<Artist> topArtist;
    public static List<Integer> helper;

    public static void main(String argv[]) throws SQLException, ClassNotFoundException, IOException {
        try {
            bigAlbumList = new ArrayList<Album>();
            bigArtistList = new ArrayList<Artist>();
            bigChartList = new ArrayList<Chart>();
            /* COMPULSORY */

            System.out.println("...Doing the COMPULSORY, TRIVIAL PART OF THE PROGRAM...\n");
            ArtistController artists = new ArtistController();
            AlbumController albums = new AlbumController();
            EntityController entityController = new EntityController();

            albums.setArtistController(artists);
            albums.clear();
            artists.clear();
            entityController.clear();

            artists.create("Radiohead", "United Kingdom");
            artists.create("Phoenix", "Romania");

            Database.commit();
            int phoenixId = artists.findByName("Phoenix");
            int radioHeadId = artists.findByName("Radiohead");

            bigArtistList.add(new Artist(phoenixId,"Radiohead", "United Kingdom"));
            bigArtistList.add(new Artist(radioHeadId,"Phoenix", "Romania"));

            //new BarChartReport(bigArtistList);

            System.out.println("...That's almost done, wait a bit...\n");
            albums.create(phoenixId, "Mugur de fluier", 1974);
            albums.create(radioHeadId, "OK Computer", 1997);
            albums.create(radioHeadId, "Kid A", 2000);
            albums.create(radioHeadId, "In Rainbows", 2007);

            bigAlbumList.add(new Album(phoenixId, "Mugur de fluier", 1974));
            bigAlbumList.add(new Album(radioHeadId, "OK Computer", 1997));
            bigAlbumList.add(new Album(radioHeadId, "Kid A", 200));
            bigAlbumList.add(new Album(radioHeadId, "In Rainbows", 2007));
            /* END COMPULOSRY */


            /* BONUS */

            // CREATE CHARTS
            Chart chart1 = new Chart();
            List<Album> albumList = new ArrayList<Album>();
            albumList.add(new Album(albums.findByName("OK Computer"),"OK Computer",1997));
            albumList.add(new Album(albums.findByName("In Rainbows"),"In Rainbows",2007));
            albumList.add(new Album(albums.findByName("Mugur de fluier"),"Mugur de fluier",1974));
            chart1.setAlbumList(albumList);

            Chart chart2 = new Chart();
            albumList = new ArrayList<Album>();
            albumList.add(new Album(albums.findByName("Kid A"),"Kid A",2000));
            albumList.add(new Album(albums.findByName("Mugur de fluier"),"Mugur de fluier",1974));
            albumList.add(new Album(albums.findByName("In Rainbows"),"In Rainbows",2007));
            albumList.add(new Album(albums.findByName("OK Computer"),"OK Computer",1997));
            chart2.setAlbumList(albumList);

            bigChartList.add(chart1);
            bigChartList.add(chart2);
            // END CREATE CHARTS

            generateData(artists,albums,entityController);


            // CREATE, READ, UPDATE, DELETE CHARTS FROM DATABASE
            entityController.createChart(chart1);
            entityController.createChart(chart2);

            Chart chart3 = new Chart();
            chart3 = entityController.readChart(1);
            entityController.updateChart(1,chart3);
            entityController.deleteChart(1);
            // END C.R.U.D.


            listRanking(albums);

            albums.list(radioHeadId);
            Database.commit();
            Database.closeConnection();
        } catch (SQLException e) {
            System.err.println(e);
            Database.rollback();
        }
    }

    public static void generateData(ArtistController artists, AlbumController albums, EntityController entityController) throws SQLException, ClassNotFoundException, IOException {

        /* CREATE FAKE DATA */
        System.out.println("GENERATING SOME FAKE DATA \n");
        Random random = new Random();
        Faker faker = new Faker();
        String[] countryCodes = Locale.getISOCountries();
        int rand, howMany, artistID;
        String name, country, albumName;
        for(int i=0; i<20;i++){

            // NEW ARTIST
            rand = random.nextInt(countryCodes.length-1);
            name = faker.artist().name();
            country = new Locale("", countryCodes[rand]).getDisplayCountry();

            artists.create(name, country);

            // END ARTIST CREATE

            // CREATE ALBUMS FOR ARTIST

            artistID = artists.findByNameLast(name);
            bigArtistList.add(new Artist(artistID,name, country));
            System.out.println("Added new artist: ID:" +artistID+ " Name:"+ name + " Country:" + country);

            howMany = random.nextInt(10-4) + 4;
            for(int j=1;j<=howMany;j++){

                rand = random.nextInt(2018-1800) + 1800;
                if(howMany%2==0)
                    albumName = faker.ancient().hero();
                else
                    albumName = faker.zelda().character();
                albums.create(artistID,albumName,rand);
                System.out.println("     -> new Album: "+albumName + " Year: "+rand);
                bigAlbumList.add(new Album(artistID,albumName,rand));
            }
            System.out.println("\n");
            //END CREATE ALBUMS
        }

        System.out.println("\n\n NOW SOME CHARTS \n\n");

        // CREATE CHARTS

        int howManyCharts = random.nextInt(15-10) + 10;
        int it=1;
        for(int k=1;k<howManyCharts;k++){

            Chart chart = new Chart();
            List<Album> albumList = new ArrayList<Album>();
            System.out.println("new Chart "+it+++" with Albums: ");
            for(int j=1;j<=10;j++){
                rand = random.nextInt(bigAlbumList.size()-1);
                Album alb = bigAlbumList.get(rand);
                while(albumList.contains(alb)){
                    rand = random.nextInt(bigAlbumList.size()-1);
                    alb = bigAlbumList.get(rand);
                }
                albumList.add(alb);
                System.out.println("     -> Album: "+alb.getName() + " ID: "+alb.getId()+" Year: "+alb.getRelase_year());
            }
            chart.setAlbumList(albumList);
            entityController.createChart(chart);
            bigChartList.add(chart);
            System.out.println("\n");
        }
        System.out.println("\nDONE ADDING FAKE DATA \n\n");
        /* END CREATE CHARTS */

        /* END CREATE FAKE DATA */
    }

    public static void listRanking(AlbumController albumController) throws SQLException, ClassNotFoundException, IOException {
        Map<Artist,Integer> statistics = new HashMap<Artist,Integer>();
        System.out.println("\n...Sorting the list of artists...\n");
        bigArtistList.sort(new Comparator<Artist>() {
            @Override
            public int compare(Artist o1, Artist o2) {
                if(o1.getId() < o2.getId()) return -1;
                else if (o1.getId() > o2.getId()) return 1;
                return 0;
            }
        });
        System.out.println("\n...Computing how many times their albums were on charts...\n");
        int it=1;
        for(Chart chart: bigChartList) {
            System.out.println("....looking at Chart "+(it++)+".");
            for (Album album : chart.getAlbumList()) {
                int artistID = albumController.getArtistID(album.getId());
                Artist artist = bigArtistList.get(artistID);
                if (statistics.containsKey(artist))
                    statistics.put(artist, statistics.get(artist) + 1);
                else
                    statistics.put(artist, 1);
            }
        }
        statistics = sortByValue(statistics);
        printTOP(statistics);

    }

    public static void printTOP(Map mp) throws IOException {
        topArtist = new ArrayList<Artist>();
        helper = new ArrayList<Integer>();
        System.out.println("\nTOP in charts:\n");
        Iterator it = mp.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            topArtist.add((Artist) pair.getKey());
            helper.add((Integer) pair.getValue());
            System.out.println(pair.getKey() + " has " + pair.getValue()+" appearances in the charts.");
            it.remove();
        }
        System.out.println("\n==================\n");
        HTMLReporter htmlReporter = new HTMLReporter(topArtist,helper);
        htmlReporter.create("MARE_SMECHERIE");
    }


    public static void printMap(Map mp) {
        Iterator it = mp.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
            it.remove();
        }
    }

    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
        List<Map.Entry<K, V>> list = new ArrayList<>(map.entrySet());
        list.sort(new Comparator<Map.Entry<K, V>>() {
            @Override
            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                if(o1.getValue().compareTo(o2.getValue()) == -1 ) return 1;
                else if(o1.getValue().compareTo(o2.getValue()) == 1 ) return -1;
                return 0;
            }
        });

        Map<K, V> result = new LinkedHashMap<>();
        for (Map.Entry<K, V> entry : list) {
            result.put(entry.getKey(), entry.getValue());
        }

        return result;
    }

}
