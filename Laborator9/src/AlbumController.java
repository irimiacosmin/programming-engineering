import java.sql.*;

public class AlbumController {

    private ArtistController artistController;

    public void create(int artist_id, String name, int release_year) throws ClassNotFoundException, SQLException {
        Connection con = Database.getConnection();
        try (PreparedStatement pstmt = con.prepareStatement("insert into albums (name, artist_id, release_year) values (?, ?, ?)")) {
            pstmt.setString(1, name);
            pstmt.setInt(2, artist_id);
            pstmt.setInt(3, release_year);
            pstmt.executeUpdate();
        }
    }

    public void list(int radioheadId) throws ClassNotFoundException, SQLException {
        Connection con = Database.getConnection();
        try (Statement stmt = con.createStatement();)
        {
            stmt.executeQuery("select name, release_year from albums where artist_id=" + radioheadId );
            ResultSet rs = stmt.getResultSet();
            System.out.println("All the albums from artist ("+radioheadId+" "+artistController.findById(radioheadId)+"): ");
            while(rs.next()) {
                System.out.println("Album name: " + rs.getString(1) + ", relased in " + rs.getInt(2));
            }
        }
    }

    public void clear() throws ClassNotFoundException, SQLException {
        Connection con = Database.getConnection();
        try (PreparedStatement pstmt = con.prepareStatement("delete from albums where id >0")) {
            pstmt.executeUpdate();
        }
        try (PreparedStatement pstmt = con.prepareStatement("ALTER TABLE albums AUTO_INCREMENT = 1")) {
            pstmt.executeUpdate();
        }
    }

    public void setArtistController(ArtistController artistController){
        this.artistController = artistController;
    }

}
