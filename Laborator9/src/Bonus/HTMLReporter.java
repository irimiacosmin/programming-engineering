package Bonus;

import Optional.Artist;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class HTMLReporter {

    private List<Artist> top;
    private List<Integer> howMany;

    public HTMLReporter(List<Artist> top, List<Integer> howMany) {
        this.top = top;
        this.howMany = howMany;
    }

    public void create(String filename) throws IOException {
        FileWriter fileWriter = new FileWriter(filename+".html");
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        String header = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "\t<head>\n" +
                "\t\t<meta charset=\"utf-8\">\n" +
                "\t\t<title>Laborator 9 - JAVA</title>\n" +
                "\t</head>\n" +
                "\t<body>\n" +
                "\t\t<h3>Laborator 9 - JAVA</h3>\n"+
                "\t\t<ul>\n" +
                "\t\t\t<li>TOP ARTISTS</li>\n" +
                "\t\t\t<ol>\n";

        int i=0;
        for(Artist artist : top){
            header = header + itemToHTML(artist,howMany.get(i));
            i++;
        }

        String footer = "\n</ol>\t\t\t\t\t\n" +
                "\t\t</ul>\t\t\n" +
                "\t</body>\n" +
                "</html>\t";


        bufferedWriter.write(header+footer);
        bufferedWriter.close();
        fileWriter.close();

        System.out.println("\n\nHTML REPORT WITH NAME: "+filename+ ".html was created.\n");

    }

    public String itemToHTML(Artist artist, int count){

        String name = artist.getName();
        String country = artist.getCountry();
        String how = String.valueOf(count);
        String model = "\n\t\t\t\t<li>Name: "+name+"</li>\n" +
                "\t\t\t\t<ol>\n" +
                "\t\t\t\t\t<li>Country: "+country+"</li>\n" +
                "\t\t\t\t\t<li>Appearances in the charts: "+how+"</li>\n";

        return model + "\t\t\t\t</ol>";
    }

}
