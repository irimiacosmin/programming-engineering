package Bonus;

import static net.sf.dynamicreports.report.builder.DynamicReports.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import Optional.Artist;
import net.sf.dynamicreports.examples.Templates;
import net.sf.dynamicreports.report.builder.column.TextColumnBuilder;
import net.sf.dynamicreports.report.builder.style.FontBuilder;
import net.sf.dynamicreports.report.datasource.DRDataSource;
import net.sf.dynamicreports.report.exception.DRException;
import net.sf.jasperreports.engine.JRDataSource;

public class BarChartReport {

    private List<Artist> top;

    public BarChartReport(List<Artist> top) {
        this.top = top;
        build();
    }

    public BarChartReport(){
        build();
    }

    private void build() {
        FontBuilder boldFont = stl.fontArialBold().setFontSize(12);

        TextColumnBuilder<String> itemColumn = col.column("no", "artists", type.stringType());
        TextColumnBuilder<Integer> quantityColumn = col.column("no", "no", type.integerType());

        try {
            report()
                    .setTemplate(Templates.reportTemplate)
                    .columns(itemColumn, quantityColumn)
                    .title(Templates.createTitleComponent("BarChart"))
                    .summary(
                            cht.barChart()
                                    .setTitle("Top Artists")
                                    .setTitleFont(boldFont)
                                    .setCategory(itemColumn)
                                    .series(
                                            cht.serie(quantityColumn))
                                    .setCategoryAxisFormat(
                                            cht.axisFormat().setLabel("Artist")))
                    .pageFooter(Templates.footerComponent)
                    .setDataSource(createDataSource())
                    .show();
        } catch (DRException e) {
            e.printStackTrace();
        }
    }

    private JRDataSource createDataSource() {
        DRDataSource dataSource = new DRDataSource("artists", "no");
        for(Artist artist: top) {
            dataSource.add(artist.getName(), 1);
        }
        return dataSource;
    }
}
