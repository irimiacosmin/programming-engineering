package Laborator1.packet_one.ex2;

import java.util.ArrayList;
import java.util.Scanner;

public class Ex2 {

    public Ex2(){

        Scanner keyboard = new Scanner(System.in);
        ArrayList<String> date = new ArrayList<String>();


        System.out.println("Introduceti N:");
        int myint = keyboard.nextInt();
        keyboard.nextLine();
        for(int i=1;i<=myint;i++){

            System.out.println("Introduceti [Nume][Nota Lab][Nota Proiect][Nota Examen][Nota Finala]");
            String str = keyboard.nextLine();
            date.add(str);

        }

        int max=0,maxCopil=0;

        for(String elev:date){

            String[] parts = elev.split(",");
            if(elev.length()>max)max=elev.length();

            if(parts[0].length()>maxCopil)maxCopil=parts[0].length();
        }

        for(int i=1;i<=max+28+maxCopil;i++)
            System.out.print("*");
        System.out.println("");

        System.out.print("* Nume ");
        for(int i=1;i<=maxCopil-4;i++)
            System.out.print(" ");
        System.out.print("* Lab * Proiect * Examen * Nota Finala *");

        System.out.println("");
        for(int i=1;i<=max+28+maxCopil;i++)
            System.out.print("*");
        System.out.println("");

        for(String elev:date){

            String[] parts = elev.split(",");

            if (parts[0].length() < 3) parts[0] = "INVALID";
            if (parts[1].length() == 0) parts[1] = "0";
            if (parts[2].length() == 0) parts[2] = "0";
            if (parts[3].length() == 0) parts[3] = "0";
            if (parts[4].length() == 0) parts[4] = "0";

            System.out.print("* "+parts[0]);
            for(int i=1;i<=maxCopil-parts[0].length();i++)
                System.out.print(" ");

            System.out.print(" * "+parts[1]+"   * "+parts[2]+"       *   "+parts[3]+"    *      "+parts[4]+"      *\n");
        }

        for(int i=1;i<=max+28+maxCopil;i++)
            System.out.print("*");
        System.out.println("");


    }

}
