package Laborator1.packet_one.ex1;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Ex1 {

    public static List<Tester> listaTesteri = new ArrayList<Tester>();
    public static List<Programator> listaProgramatori = new ArrayList<Programator>();
    public static List<Sef_de_Echipa> listaSE = new ArrayList<Sef_de_Echipa>();
    public static List<Sef_de_Grupa> listaSG = new ArrayList<Sef_de_Grupa>();

    public static void main(String[] args){

        Ex1 ex1 = new Ex1();

        /////////// SERIALIZARE

        for (Tester emp:listaTesteri) {
            new Serializare(emp);
        }
        for (Programator emp:listaProgramatori) {
            new Serializare(emp);
        }
        for (Sef_de_Echipa emp:listaSE) {
            new Serializare(emp);
        }
        for (Sef_de_Grupa emp:listaSG) {
            new Serializare(emp);
        }
        /////////// END SERIALIZARE


        /////////// DESERIALIZARE
        Programator y1 = Serializare.deserializareProgramator("1972.ser");
        Tester y2 = Serializare.deserializareTester("2973.ser");
        // System.out.println(y1.nume+" "+y1.prenume+" "+y1.limbaje_de_programare[0]);
        // System.out.println(y2.nume+" "+y2.prenume+" "+y2.TestareAutomata);

        /////////// END DESERIALIZARE


    }


    public  Ex1(){



        Tester t1 = new Tester(2971,"Trinca","Ioana-Alexandra",true);
        Tester t2 = new Tester(2972,"Popescu","Ion",false);
        Tester t3 = new Tester(2973,"Adamescu","Florin",true);

        Programator p1 = new Programator(1971,"Matei","Madalin", new String[]{"Java", "C++"});
        Programator p2 = new Programator(1972,"Ouatu","Bogdan", new String[]{"Java", "C++","Python"});
        Programator p3 = new Programator(1974,"Pohomohaci","Dumitru", new String[]{"C++"});
        Programator p4 = new Programator(1975,"Worst","Developer", new String[]{"PHP"});

        Sef_de_Echipa s1 = new Sef_de_Echipa(1973,"Irimia","Cosmin-Iulian");
        s1.setSubordinates(new ArrayList<Angajat>() {{add(t1);add(p1);add(p3);}});

        Sef_de_Echipa s2 = new Sef_de_Echipa(1976,"Bucevschi","Alexandru-Gabriel");
        s2.setSubordinates(new ArrayList<Angajat>() {{add(t2);add(t3);add(p2);add(p4);}});

        Sef_de_Grupa boss = new Sef_de_Grupa(1978,"Vintur","Cristian");
        boss.setSubordinates(new ArrayList<Sef_de_Echipa>() {{add(s2);add(s1);}});

        Manager patron = new Manager(2000,"Iftene","Adrian");
        patron.setSubordinates(new ArrayList<Angajat>() {{add(t1);add(t2);add(t3);add(p1);add(p2);add(p3);add(p4);add(s1);add(s2);add(boss);}});

        listaTesteri.add(t1);
        listaTesteri.add(t2);
        listaTesteri.add(t3);
        listaProgramatori.add(p1);
        listaProgramatori.add(p2);
        listaProgramatori.add(p3);
        listaSE.add(s1);
        listaSE.add(s2);
        listaSG.add(boss);
        new Serializare(patron);

       System.out.println(t1.getString());
        System.out.println(t2.getString());
        System.out.println(t3.getString());

        System.out.println(p1.getString());
        System.out.println(p2.getString());
        System.out.println(p3.getString());
        System.out.println(p4.getString());


        System.out.println(s1.getString());
        System.out.println(s2.getString());

        System.out.println(boss.getString());
        System.out.println(patron.getString());
    }

}
