package Laborator1.packet_one.ex1;

import java.io.Serializable;
import java.util.List;

public class Manager extends Angajat {

    private List<Angajat> subordinates;

    public List<Angajat> getSubordinates() {
        return subordinates;
    }

    public void setSubordinates(List<Angajat> subordinates) {
        this.subordinates = subordinates;
    }

    public Manager(int CNP, String nume, String prenume){

        super(CNP,nume,prenume);

    }
    public String getString(){
        return "[ZEU] CNP:"+this.CNP+"  Nume: "+this.nume+ " "+this.prenume+"   Subordonati: TOATA LUMEA!!!";
    }

}