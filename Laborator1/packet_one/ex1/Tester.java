package Laborator1.packet_one.ex1;

public class Tester extends Angajat{

    boolean TestareAutomata;

    public Tester(int CNP, String nume, String prenume,Boolean TestareAutomata)
    {
        super(CNP, nume, prenume);
        this.TestareAutomata = TestareAutomata;
    }

    public String getString(){
        return "[Tester] CNP:"+this.CNP+"  Nume: "+this.nume+ " "+this.prenume+"   Testare Automata: "+this.TestareAutomata;
    }
}
