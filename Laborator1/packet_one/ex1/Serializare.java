package Laborator1.packet_one.ex1;

import java.io.*;

public class Serializare{

    public Serializare(Tester e)
    {
        try {
            FileOutputStream fileOut =
                    new FileOutputStream("C:\\Users\\Cosmin.DESKTOP-9QN1QUN\\IdeaProjects\\untitled\\src\\Laborator1\\packet_one\\tmp\\"+(int)e.CNP+".ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(e);
            out.close();
            fileOut.close();
        } catch (Exception i) {

        }
    }

    public Serializare(Programator e){
        try {
            FileOutputStream fileOut =
                    new FileOutputStream("C:\\Users\\Cosmin.DESKTOP-9QN1QUN\\IdeaProjects\\untitled\\src\\Laborator1\\packet_one\\tmp\\"+(int)e.CNP+".ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(e);
            out.close();
            fileOut.close();
        } catch (Exception i) {

        }
    }

    public Serializare(Sef_de_Echipa e)
    {
        try {
            FileOutputStream fileOut =
                    new FileOutputStream("C:\\Users\\Cosmin.DESKTOP-9QN1QUN\\IdeaProjects\\untitled\\src\\Laborator1\\packet_one\\tmp\\"+(int)e.CNP+".ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(e);
            out.close();
            fileOut.close();
        } catch (Exception i) {

        }
    }

    public Serializare(Sef_de_Grupa e)
    {
        try {
            FileOutputStream fileOut =
                    new FileOutputStream("C:\\Users\\Cosmin.DESKTOP-9QN1QUN\\IdeaProjects\\untitled\\src\\Laborator1\\packet_one\\tmp\\"+(int)e.CNP+".ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(e);
            out.close();
            fileOut.close();
        } catch (Exception i) {

        }
    }

    public Serializare(Angajat e)
    {
        try {
            FileOutputStream fileOut =
                    new FileOutputStream("C:\\Users\\Cosmin.DESKTOP-9QN1QUN\\IdeaProjects\\untitled\\src\\Laborator1\\packet_one\\tmp\\"+(int)e.CNP+".ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(e);
            out.close();
            fileOut.close();
        } catch (Exception i) {

        }
    }

    public Serializare(Manager e)
    {
        try {
            FileOutputStream fileOut =
                    new FileOutputStream("C:\\Users\\Cosmin.DESKTOP-9QN1QUN\\IdeaProjects\\untitled\\src\\Laborator1\\packet_one\\tmp\\"+(int)e.CNP+".ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(e);
            out.close();
            fileOut.close();
        } catch (Exception i) {

        }
    }


    public static Tester deserializareTester(String filename){
        Tester e = null;
        try {
            FileInputStream fileIn = new FileInputStream("C:\\Users\\Cosmin.DESKTOP-9QN1QUN\\IdeaProjects\\untitled\\src\\Laborator1\\packet_one\\tmp\\"+filename);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            e = (Tester) in.readObject();
            in.close();
            fileIn.close();
        } catch (Exception i) {

        }

        return e;
    }

    public static Programator deserializareProgramator(String filename){
        Programator e = null;
        try {
            FileInputStream fileIn = new FileInputStream("C:\\Users\\Cosmin.DESKTOP-9QN1QUN\\IdeaProjects\\untitled\\src\\Laborator1\\packet_one\\tmp\\"+filename);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            e = (Programator) in.readObject();
            in.close();
            fileIn.close();
        } catch (Exception i) {

        }

        return e;
    }

    public static Sef_de_Echipa deserializareSef_de_echipa(String filename){
        Sef_de_Echipa e = null;
        try {
            FileInputStream fileIn = new FileInputStream("C:\\Users\\Cosmin.DESKTOP-9QN1QUN\\IdeaProjects\\untitled\\src\\Laborator1\\packet_one\\tmp\\"+filename);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            e = (Sef_de_Echipa) in.readObject();
            in.close();
            fileIn.close();
        } catch (Exception i) {

        }

        return e;
    }

    public static Sef_de_Grupa deserializareSef_de_Grupa(String filename){
        Sef_de_Grupa e = null;
        try {
            FileInputStream fileIn = new FileInputStream("C:\\Users\\Cosmin.DESKTOP-9QN1QUN\\IdeaProjects\\untitled\\src\\Laborator1\\packet_one\\tmp\\"+filename);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            e = (Sef_de_Grupa) in.readObject();
            in.close();
            fileIn.close();
        } catch (Exception i) {

        }

        return e;
    }

    public static Manager deserializareManager(String filename){
        Manager e = null;
        try {
            FileInputStream fileIn = new FileInputStream("C:\\Users\\Cosmin.DESKTOP-9QN1QUN\\IdeaProjects\\untitled\\src\\Laborator1\\packet_one\\tmp\\"+filename);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            e = (Manager) in.readObject();
            in.close();
            fileIn.close();
        } catch (Exception i) {

        }

        return e;
    }




}
