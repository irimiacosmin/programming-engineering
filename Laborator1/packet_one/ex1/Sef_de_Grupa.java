package Laborator1.packet_one.ex1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Sef_de_Grupa extends Angajat {

    private List<Sef_de_Echipa> subordinates;

    public List<Sef_de_Echipa> getSubordinates() {
        return subordinates;
    }

    public void setSubordinates(List<Sef_de_Echipa> subordinates) {
        this.subordinates = subordinates;
    }

    public Sef_de_Grupa(int CNP, String nume, String prenume){

        super(CNP,nume,prenume);
    }

    public String getString(){

        List<Sef_de_Echipa> arr = new ArrayList<Sef_de_Echipa>();
        arr = getSubordinates();

        String str = "[Sef de grupa] CNP:"+this.CNP+"  Nume: "+this.nume+ " "+this.prenume+"   Subordonati: [ ";
        for(Sef_de_Echipa limbaj:arr){
            str = str+limbaj.nume+" "+limbaj.prenume+"("+limbaj.CNP+")  ";
        }
        return str+"]";
    }

}
