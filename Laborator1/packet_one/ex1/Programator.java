package Laborator1.packet_one.ex1;

import java.util.ArrayList;

public class Programator extends Angajat {

    String[] limbaje_de_programare;

    public Programator(int CNP, String nume, String prenume,String[] limbaje_de_programare)
    {
        super(CNP, nume, prenume);
        this.limbaje_de_programare = limbaje_de_programare;
    }

    public String getString(){
        String str = "[Programator] CNP:"+this.CNP+"  Nume: "+this.nume+ " "+this.prenume+"   Limbaje cunoscute: [ ";
        for(String limbaj:limbaje_de_programare){
            str = str+limbaj+" ";
        }
        return str+"]";
    }

}
