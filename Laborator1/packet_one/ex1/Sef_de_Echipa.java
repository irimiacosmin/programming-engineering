package Laborator1.packet_one.ex1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Sef_de_Echipa extends Angajat {

    private List<Angajat> subordinates;

    public List<Angajat> getSubordinates() {
        return subordinates;
    }

    public void setSubordinates(List<Angajat> subordinates) {
        this.subordinates = subordinates;
    }

    public Sef_de_Echipa(int CNP, String nume, String prenume){

        super(CNP,nume,prenume);
    }

    public String getString(){

        List<Angajat> arr = new ArrayList<Angajat>();
        arr = getSubordinates();

        String str = "[Sef de echipa] CNP:"+this.CNP+"  Nume: "+this.nume+ " "+this.prenume+"   Subordonati: [ ";
        for(Angajat limbaj:arr){
            str = str+limbaj.nume+" "+limbaj.prenume+"("+limbaj.CNP+")  ";
        }
        return str+"]";
    }

}
