package com.company.portofolio;

import java.util.List;

public interface Algorithm {

    List<Item> computeSolution(List<Item> assetList, int maxValue);

}
