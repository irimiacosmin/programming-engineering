package com.company.portofolio;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AssetManager {

    List<Item> itemList;

    public AssetManager() {
        itemList = new ArrayList<Item>();
    }

    public void add(Item ... items){
        for(Item item: items)
            itemList.add(item);
    }

    public List<Item> getItems(){

        Collections.sort(itemList, new Comparator<Item>() {
            @Override
            public int compare(Item o1, Item o2) {
                return o1.name.compareTo(o2.name);
            }
        });

        return itemList;
    }

    public List<Item> getAssets(){

        List<Item> assetList = new ArrayList<Item>();
        for(Item item: itemList)
            if(item instanceof Asset)
                assetList.add(item);

        Collections.sort(assetList, new Comparator<Item>() {
            @Override
            public int compare(Item o1, Item o2) {
                double profitA = ((Asset) o1).computeProfit() ;
                double profitB = ((Asset) o2).computeProfit() ;
                if(profitA == profitB) return 0;
                if(profitA > profitB) return 1;
                return -1;
            }
        });

        return assetList;
    }

    public Portofolio createPortofolio(Algorithm algorithm, int maxValue){
        return new Portofolio(algorithm.computeSolution(getAssets(),maxValue));
    }

}
