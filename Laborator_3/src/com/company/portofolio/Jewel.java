package com.company.portofolio;

public class Jewel extends Item {


    public Jewel(String name, int price) {

        super(name,price);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Jewel{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
