package com.company.portofolio;

import java.util.Random;

public interface Asset {

    double computeProfit();
    default double riskFactor(){
        return 0.45;
    }
}
