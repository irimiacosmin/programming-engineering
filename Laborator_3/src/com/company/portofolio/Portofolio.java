package com.company.portofolio;

import java.util.List;

public class Portofolio{

    List<Item> itemList;

    public Portofolio(List<Item> itemList) {
        this.itemList = itemList;
    }

    @Override
    public String toString() {
        return "Portofolio{" +
                "itemList=" + itemList +
                '}';
    }
}
