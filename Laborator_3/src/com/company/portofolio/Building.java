package com.company.portofolio;

import java.util.Random;

public class Building extends Item implements Asset {

    private int area;

    public Building(String name, int area, int price) {

        super(name,price);
        this.area = area;

    }

    public double computeProfit(){
        return area/price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    @Override
    public String toString() {
        return "Building{" +
                "name='" + name + '\'' +
                ", price='" + price + '\'' +
                ", area=" + area +
                '}';
    }

}
