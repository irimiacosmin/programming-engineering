package com.company.portofolio;

public class Vehicle extends Item implements Asset{

    private int performance;

    public Vehicle(String name,int performance, int price) {

        super(name,price);
        this.performance = performance;

    }
    public double computeProfit(){
        return performance/price;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getPerformance() {
        return performance;
    }

    public void setPerformance(int performance) {
        this.performance = performance;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "name='" + name + '\'' +
                ", price=" + price +
                ", performance=" + performance +
                '}';
    }
}
