package com.company.staff;

import java.util.ArrayList;
import java.util.List;

public class Problem {

    private List<Student> students;
    private List<Teacher> teachers;


    public Problem(){

        students = new ArrayList<Student>();
        teachers = new ArrayList<Teacher>();

    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(Student ... students) {
        for(Student student : students) {
            this.students.add(student);
        }
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public void setTeachers(Teacher ... teachers) {
        for(Teacher teacher : teachers) {
            this.teachers.add(teacher);
        }
    }

    @Override
    public String toString() {
        return "Problem{" +
                "students=" + students +
                ", teachers=" + teachers +
                '}';
    }
}
