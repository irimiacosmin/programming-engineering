package com.company.staff;

import java.util.ArrayList;
import java.util.List;

public class Student {

    private String name;
    private String email;
    private List<Project> preferences;

    public Student(String name, String email) {

        this.name = name;
        this.email = email;
        preferences = new ArrayList<Project>();

    }

    public void setPreferences(Project ... projects) {

        for(Project project : projects) {

            this.preferences.add(project);

        }

    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Project> getPreferences() {
        return preferences;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", preferences=" + preferences +
                '}';
    }
}
