package com.company.staff;

import java.util.ArrayList;
import java.util.List;

public class Teacher {

    private String name;
    private String email;
    private List<Student> preferences;

    public Teacher(String name, String email) {

        this.name = name;
        this.email = email;
        preferences = new ArrayList<Student>();

    }

    public void setPreferences(Student ... students) {

        for(Student student : students) {

            this.preferences.add(student);

        }

    }

    public Project createProject(String ID, int capacity){
        return new Project(ID,capacity);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<Student> getPreferences() {
        return preferences;
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", preferences=" + preferences +
                '}';
    }
}
